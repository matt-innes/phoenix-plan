
# Resource macros

# Assign a random developer - testing purposes only
macro allocateDeveloper [
  allocate glen { alternative james, ted, matt, guy, phil, martyn  persistent }
]

# Assign someone other than glen that is capable of doing server work
macro allocateServerDev [
  allocate james { alternative matt, martyn, ted  persistent }
]

# Non-glen resource for logic work
macro logicMiscResource [
  allocate martyn { alternative james, matt persistent }
]

# Resources for firmware work
macro moduleMiscResource [
  allocate glen { alternative matt persistent }
]

# Resources for device support work
macro deviceSupportResource [
  # allocate unassigned
  allocate glen
]

# Effort macros

# Default effort - testing purposes only
macro defaultEffort [
  effort 4d
]

# Default small effort - testing purposes only
macro defaultSmallEffort [
  effort 1d
]

# Random default development - testing purposes only
macro defaultDevelopment [
  ${allocateDeveloper}
  ${defaultEffort}
]

# Macro for task tip on project plans
macro TaskTip [
  tooltip istask() -8<-
    '''Start: ''' <-query attribute='start'->
    '''End: ''' <-query attribute='end'->
    ----
    '''Resources:'''

    <-query attribute='resources'->
    ----
    '''Precursors: '''

    <-query attribute='precursors'->
    ----
    '''Followers: '''

    <-query attribute='followers'->
    ->8-
]

# Columns to show on task lists (project plan lite)
macro taskListColumns [
    #columns bsi, name, Jira, resources, chart { ${TaskTip} }
    columns bsi, name, effort, resources { ${narrowColumn} }, scenario, chart { ${TaskTip} ${wideColumn} }
]

# Columns to show on project plans
macro projectPlanColumns [
    #columns bsi, name, Jira, resources, chart { ${TaskTip} }
    columns bsi, name, effort, resources { ${narrowColumn} }, scenario, chart { ${TaskTip} ${wideColumn} }
]

macro dropColumns [
    columns bsi, name, resources
    sorttasks tree
]

# Scenario to use as the baseline for project plans
#macro baselinePlan[base, actual]
macro baselinePlan[base]

# Which scenarios to use in multiscenario reports
macro multiScenarios[scenarios ${baselinePlan}]

# Default values for drop2 tasks
macro drop2 [
  flags drop2
  priority 600
]

# Default values for drop3 tasks
macro drop3 [
  flags drop3
  priority 400
]

# Target due dates for drops
macro drop1Date [2016-06-27]
macro drop2Date [2016-08-26]
macro drop3Date [2016-09-30]
macro finalDate [2016-10-19]

# Width of a wide column (forcing report to use the whole page)
macro wideColumn [
  width 1200
]

# Width of a narrow column (to prevent wide columns from taking over the whole page)
macro narrowColumn [
  width 300
]

