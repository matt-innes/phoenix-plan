task phoenix "Project Phoenix" {

    task block_out "Development for June release" {
      priority 1000
      note "In-flight work to deliver mid-June release for Aggreko"
      start ${projectstart}
      # Is this date still correct?
      end 2016-6-10
      allocate glen, james, guy, phil
    }

    task hver "Hardware specification" {
      priority 750
      flags specification, hardware
      # Jira "https://telemisis.atlassian.net/browse/TEL-24" { label "TEL-24" }
      #${jiraRef "TEL-24"}
      #task rut950 "RUT950 router investigation" {
      #  allocate tom
      #  effort 9d
      #  ${jiraRef "TEL-35" }
      #  flags drop1
      #}
      #task rver "Router verification & specification" {  depends !rut950 flags drop1 }
      task fver "Fan controller verification & specification"
      task encl "Enclosure design" { allocate iain ${defaultEffort}  note "Is this still needed or are Inala handling?" }
      task int_pcb "Design integration PCB for enclosure"
      task wiring "Create site wiring diagrams" { priority 250 allocate iain ${defaultEffort} }
    }

    task misc_hw "Miscellaneous hardware tasks" {
      priority 550
      flags hardware, drop1
      task "Analyse peripherals testing requirements" { flags specification allocate tony ${defaultSmallEffort} note "Third party verification" ${jiraRef "TEL-36"} complete 0 }
      task "Test multiple RS-485 devices of different types on the same bus" {
        flags testing
        allocate guy
        ${jiraRef "TEL-37"}
        effort 2w
        complete 90
      }
    }

    task infrastructure "Infrastructure tasks" {
      priority 550
      flags drop1
      allocate james { alternative mike persistent }
      task external "Create externally-facing v5 test instance" { effort 3d ${jiraRef "TEL-38"} complete 20 }
      task internal "Create internal v5 R&D instance" { effort 3d ${jiraRef "TEL-39"} complete 20 }
    }

    task rest "REST service implementation" {
      flags development, server
      allocate matt
      ${jiraRef "TEL-25"}
      task json "Define JSON format and specify services" {
        note "No overlap with MQTT definitions"
        effort 3d
        flags drop1
      }
      task mqtt_support "Services to support MQTT" {
        note "REST services to support MQTT messaging; for example get object by object id"
        depends !json
        effort 5d
        flags drop2
      }
      task ui "MQTT configuration UI" { effort 5d flags drop3 }
      task configuration "Services to support configuration" {
        note "Services to allow IMQS to configure SitePro without using UI"
        depends !json
        effort 10d
        flags drop2
      }
    }

    task buf "Data and alarm buffering" {
      ${jiraRef "TEL-26"}
      task buf_spec "Specify data and alarm buffering" {
        flags firmware, server, specification, drop1
        allocate ted
        effort 2w
        complete 80
        ${jiraRef "TEL-40"}
      }
      # This is now incorrect following the breaking of buffering into phases
      task buf_server "Develop server-side data and alarm buffering" {
        flags server, development, drop2
        depends !buf_spec
        allocate ted
        task "Warnings and precompiled header. Check warning level 4" { effort 1d priority 630 ${jiraRef "TEL-41"} }
        task "Design interface changes between module and server" { effort 4d priority 620 ${jiraRef "TEL-42"} }
        task "Design database changes (Configuration and new flags in tables)" { effort 4d priority 610 ${jiraRef "TEL-43"} }
        task "Messages" {
          task "Creation of new server messages" { effort 2.5d priority 600 ${jiraRef "TEL-44"} }
          task "Changes to INIT message" { effort 1.5d priority 590 ${jiraRef "TEL-45"} }
          task "Updates to changed server messages" {
            ${jiraRef "TEL-46"}
            task "Configuration messages" { effort 1.5d priority 580 ${jiraRef "TEL-47"} }
            task "All messages (timestamp, message id etc)" { effort 2.5d priority 570 ${jiraRef "TEL-48"} }
          }
        }
        task "Update to handling of updated module messages" {
          task "Message accumulator – update" {
            ${jiraRef "TEL-49"}
            task "Investigate" { effort 4d priority 560 ${jiraRef "TEL-50"} }
            task "Implementation (dependent on investigation)" { effort 6.5d priority 550 ${jiraRef "TEL-51"} }
          }
          task "Fuel handling" { effort 1.5d priority 540 ${jiraRef "TEL-52"} }
          task "Database persistence" {
            ${jiraRef "TEL-3"}
            task "Alarms" { effort 2.5d priority 530 ${jiraRef "TEL-54"} }
            task "Other" { effort 1.5d priority 520 ${jiraRef "TEL-55"} }
          }
          task "Triggering and storage of data scheduling" { effort 4d priority 510 ${jiraRef "TEL-56"} }
        }
      }
      task buf_firm "Develop firmware data and alarm buffering" {
        flags server, development, drop2
        depends !buf_spec
        effort 2w
        allocate glen
      }
    }

    task comms "Connections/communications management" {
      priority 600
      allocate glen
      ${jiraRef "TEL-27"}
      task spec "Specify communications management" {
        flags specification, drop1
        effort 5d
      }
      task rework "Connections/communications management rework" {
        note "Comms in firmware rework with associated server side changes"
        depends !spec
        flags development, drop2

        task chan_sel "Rework communications channel selection" {
          note "This is choosing which channel is right (SMS/data/iridium etc) based on a number of factors - network availability, cost etc"
          task "Server-side configuration" {
            effort 5d
            flags server
          }
          task "Firmware implementation" {
            flags firmware
            effort 10d
          }
        }
        #task rout_comm "Router commands to MCU" {
        #  note "This is sending configuration commands to the external router through the MCU"
        #  flags mcu, firmware
        #  effort 5d
        #}
      }
    }

    task mqtt "MQTT interface" {
      allocate matt
      flags server, development
      ${jiraRef "TEL-28"}
      note "Deliver change indications directly from the GLS (CML/CLS) processes"
      task initial "Initial delivery" {
        priority 550
        flags drop1
        task json "Define JSON format" { flags specification effort 1d complete 100 }
        task client "Implement an MQTT client instance of the broker base class" { note "AlarmPublisher.cpp in the link_server" effort 9d depends !json complete 100 }
        task alarm "Events & alarms" { depends !client  effort 5d complete 100 }
      }
      # These are just data messages now
      # task "Change of state" { depends !initial.client effort 5d flags drop2 note "e.g. change of power source, loss of comms channel" }
      task "Data messages" { depends !initial.client effort 5d flags drop2 note "Eoin's stream of values for BIS" }
      task "Configuration change" { depends !initial.client effort 5d flags drop2 note "New sites, collections etc" }
    }

    task ip "IP passthrough/external router" {
      priority 550
      flags firmware, server, development, drop2
      ${jiraRef "TEL-29"}
      note "Interactions with external 4G router and support for transparent connections"
      #task ext_router "External router interfacing" {
      #  task "Router support from SN3G" { note "such as is required" allocate guy { alternative phil persistent } effort 5d }
      #  task "Extracting data from router" { flags firmware note "May not be possible" allocate guy { alternative phil persistent } effort 5d }
      #}
      task "IP to serial" {
        task "Enhance MCU to accept multiple Modbus passthrough" { flags mcu note "IP to serial passthrough read-only"  }
        task "Supporting configuration of SEP Modbus devices (server)" { flags server effort 3d ${allocateDeveloper} }
        task "Supporting configuration of SEP Modbus devices (firmware)" { flags firmware effort 3d ${allocateDeveloper} }
        task "Support alarms from SEP MOdbus devices" { flags server effort 2d ${allocateDeveloper} }
        task "SEP devices firmware upgrade over Modbus (server)" { flags server, drop3 note "IP to serial passthrough configuration" effort 2d ${allocateDeveloper} }
        task "SEP devices firmware upgrade over Modbus (firmware)" { flags firmware, drop3 note "IP to serial passthrough configuration" effort 1d ${allocateDeveloper} }
      }
    }

    task ldu "Local diagnostic utility" {
      flags firmware, server, development, drop2
      note "Enhanced local web page on SN3G"
      ${jiraRef "TEL-30"}
      allocate phil
      task "SitePro communication status" {  effort 3d }
      task "Peripheral status" {  effort 3d }
      task "Diagnostic information" {  effort 3d }
    }

    task access "Access control" {
      flags firmware, server, development, drop3
      note "this might be simpler than this"
      ${jiraRef "TEL-31"}
      task "import of access card details into system database"
      task "method of deciding which cards should be loaded into each card reader"
      task "Modbus support for card configuration" { depends ext_dep.inala.spec_modbus_ac  }
      task "reading of entry events from reader and sending to server"
      task "admin functions on server"
      task "history of entry events"
      task "masking of intrusion etc alarms following authorised access"
    }

    task logic "Logic engine" {
      flags firmware, server
      allocate martyn
      task logic_spec "Specify power and cooling logic implementation" { flags specification, drop1 effort 2w complete 30 }
      ${jiraRef "TEL-32"}
      task logic_dev "Logic engine and configuration" {
        flags development, drop2
        depends !logic_spec
        task "data driven implementation of logic state machine in the SiteNode Java" { flags firmware allocate glen effort 2w }
        # removed following tony email 2016-05-16-14-09
        # task "configuration of logic templates in the UI" { flags server allocate martyn effort 1.5w }
        task "logic download from the GLS (CML/CLS)" { flags server allocate martyn effort 1.5w }
        task "transfer of logic state information to GLS" { flags server allocate martyn effort 1.5w }
        task "integration of logic state information into the database and for presentation purposes into the UI" { flags server allocate martyn effort 1.5w }
      }
    }

    task dev_supp "Device support" {
      ${jiraRef "TEL-33"}
      task temp "New temperature and humidity sensor" {
        flags hardware
        task "Design" { flags specification }
        task "Test" { flags testing }
      }
      flags firmware, server, development
      task "BTS alarm module" {
        task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
        task impl "Implementation" {
          depends !spec flags drop3
          task map "Mapping of local alarms to BTS outputs"
        }
      }
      task inala "Jabil/Inala devices" {
        task emu "EMU" {
          note "Equipment mains monitoring unit - AC power, measure current, voltage per phase via coils"
          depends ext_dep.inala.spec_emu
          flags firmware, server, development
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop1 }
          task impl "Implementation" { depends !spec flags drop2 }
        }
        task pfp "Pressure fuel probe" {
          flags firmware, server, development
          depends ext_dep.inala.spec_pfp
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop1 }
          task impl "Implementation" { depends !spec flags drop1 }
        }
        task mfp "Magnetorestrictive fuel probe" {
          flags firmware, server, development
          depends ext_dep.inala.spec_mfp
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
        }
        task dlc "Door lock controller" {
          flags firmware, server, development
          note "SA shelter door"
          depends ext_dep.inala.spec_dlc
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
        }
        task dpp "Digital Power Probes" {
          flags firmware, server, development
          depends ext_dep.inala.spec_dpp
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop1 }
          task impl "Basic implementation" { depends !spec flags drop1 }
        }
        task mpf "MPF" {
          flags development
          note "Mains power frequency"
          depends ext_dep.inala.spec_mpf
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop1 flags specification, firmware, mcu }
          task mcu "MCU interfacing" { flags mcu, development, drop3 depends !spec }
          task java "Java/system level support" { flags firmware, development, drop3 depends !spec }
        }
        task ac "Aircon controller" {
          flags firmware, server, development
          depends ext_dep.inala.spec_ac
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
        }
        task awl "AWL controller" {
          flags firmware, server, development
          note "Aircraft warning lights"
          depends ext_dep.inala.spec_awl
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
        }
        task sd "Smoke detector" {
          flags firmware, server, development
          depends ext_dep.inala.spec_sd
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
        }
      }
      task "water sensor" {
          note "Water in diesel"
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop2 }
      }
      task "I/O devices" {
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
      }
      task "Comap" {
          note "Generator type"
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
      }
      task "Delta" {
          note "Rectifier"
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
      }
      task "Deep Sea" {
          note "7230?"
          task spec "Device validation, specification, implementation specification" { effort 3d allocate unassigned flags drop2 }
          task impl "Implementation" { depends !spec flags drop3 }
      }
    }

    task "Jabil specific UI" {
        ${jiraRef "TEL-34"}
        note "custom grids, menus, error messages, logo, bespoke configuration,  site modelling, custom configurations, operational state models"
        task config2 "Customisation of configuration" { flags ui, configuration, drop2 }
        task model "Bespoke modelling" { flags ui, configuration, drop2 }
        task config3 "More customisation of configuration" { flags ui, configuration, drop3 }
    }
}

