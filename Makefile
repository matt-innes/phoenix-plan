# Makefile for building different versions of the project plan

# Variables
COMMON_DIR=../common

# Common dependencies
COMMON_DEPS=$(COMMON_DIR)/project.tji $(COMMON_DIR)/macros.tji external-dependencies.tji macros.tji resources.tji
DETAIL_DEPS=$(COMMON_DEPS) reports.tji
TJ3CMD=tj3
#TJ3CMD=tj3 --debug --debuglevel 4

all: hl-v3 hl-v4 v3 test

test: test/test.html
v1: v1/index.html
v2: v2/index.html
v3: v3/index.html
hl-v3: hl-v3/index.html
hl-v4: hl-v4/index.html

v1/index.html: phoenix.tjp $(DETAIL_DEPS) milestones.tji tasks.tji 
	$(TJ3CMD) --output-dir v1 $<

v2/index.html: phoenix_v2.tjp $(DETAIL_DEPS) milestones_v2.tji tasks_v2.tji 
	$(TJ3CMD) --output-dir v2 $<

v3/index.html: phoenix_v3.tjp $(DETAIL_DEPS) milestones_v3.tji tasks_v3.tji 
	$(TJ3CMD) --output-dir v3 $<

hl-v3/index.html: hlphoenix_v3.tjp $(COMMON_DEPS) hltasks_v3.tji hlreports.tji hlmilestones_v3.tji hlbookings.tji
	$(TJ3CMD) --output-dir hl-v3 $<

hl-v4/index.html: hlphoenix_v4.tjp $(COMMON_DEPS) hltasks_v4.tji hlreports_v4.tji hlmilestones_v4.tji hlbookings_v4.tji
	$(TJ3CMD) --output-dir hl-v4 $<

test/test.html: test.tjp
	$(TJ3CMD) --output-dir test $<

clean: clean-test clean-v1 clean-v2 clean-v3 clean-hl-v3 clean-hl-v4 clean-hl

clean-test:
	rm -f test/*.html
	rm -f test/*.csv
	rm -f test/*.xml
	rm -rf test/tags
	rm -rf test/icons
	rm -rf test/scripts
	rm -rf test/css

clean-v1:
	rm -f v1/*.html
	rm -f v1/*.csv
	rm -f v1/*.xml
	rm -rf v1/tags
	rm -rf v1/icons
	rm -rf v1/scripts
	rm -rf v1/css

clean-v2:
	rm -f v2/*.html
	rm -f v2/*.csv
	rm -f v2/*.xml
	rm -rf v2/tags
	rm -rf v2/icons
	rm -rf v2/scripts
	rm -rf v2/css

clean-v3:
	rm -f v3/*.html
	rm -f v3/*.csv
	rm -f v3/*.xml
	rm -rf v3/tags
	rm -rf v3/icons
	rm -rf v3/scripts
	rm -rf v3/css

clean-hl:
	rm -f hl/*.html
	rm -f hl/*.csv
	rm -f hl/*.xml
	rm -rf hl/tags
	rm -rf hl/icons
	rm -rf hl/scripts
	rm -rf hl/css

clean-hl-v3:
	rm -f hl-v3/*.html
	rm -f hl-v3/*.csv
	rm -f hl-v3/*.xml
	rm -rf hl-v3/tags
	rm -rf hl-v3/icons
	rm -rf hl-v3/scripts
	rm -rf hl-v3/css

clean-hl-v4:
	rm -f hl-v4/*.html
	rm -f hl-v4/*.csv
	rm -f hl-v4/*.xml
	rm -rf hl-v4/tags
	rm -rf hl-v4/icons
	rm -rf hl-v4/scripts
	rm -rf hl-v4/css
